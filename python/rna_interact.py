#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 21 15:09:20 2019

@author: kdhaynes
"""
# Program to predict whether two RNA sequences interact,
# using a random forest.

import csv, RNA
import Levenshtein as lev
import numpy as np

from joblib import dump, load
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV, ShuffleSplit
from sklearn.tree import export_graphviz

# ============================================================================
# Set flags
training_now = False
vaccuracy_now = False
vimportance_now = False
vbranch_now = False
parameters_now = False

# Set input filenames
frawtrain  = 'seq_training.csv'
fkmertrain = 'kmer_training.csv'
fdeeptrain = 'deepbind_training.csv'

frawtest  = 'seq_test.csv'
fkmertest = 'kmer_test.csv'
fdeeptest = 'deepbind_test.csv'

# Set output filenames
frfm = 'rna_rfm.dat'
frawresult  = 'seq_test.out'
fkmerresult = 'kmer_test.out'
fdeepresult = 'deepbind_test.out'

# Set tree visualization names
ftreedot = 'small_tree.dot'
ftreepng = 'small_tree.png'

# ============================================================================
# Obtain the strings of all possible 5-kmers as well as the index
# of their compliments (A-T, C-G).
def obtain_kmer5():
    rnachar = ['A','C','G','T']
    nkv = 4**5
    kmerchar = ['']*nkv
    kmercref = [0]*nkv

    count = 0
    for i1 in range(4):
        for i2 in range(4):
            for i3 in range(4):
                for i4 in range(4):
                    for i5 in range(4):
                        kmerchar[count] = rnachar[i1] + rnachar[i2] + \
                            rnachar[i3] + rnachar[i4] + rnachar[i5]                                
                        kmercref[count] = abs(i1-3) + abs(i2-3)*4 + \
                            abs(i3-3)*4**2 + abs(i4-3)*4**3 + abs(i5-3)*4**4
                        count += 1
    
    return kmerchar, kmercref
    
# Read in three different RNA representations and obtain features.
def obtain_features(file1, file2, file3):
    nkv=1024
    nml=100

    nfeaturesc = 25
    nfeaturest = nfeaturesc+nkv+nkv #+nml*8
    with open(file1) as f1:
         csv_reader = csv.reader(f1,delimiter=',')
         nlines = sum(1 for row in csv_reader)
    features = np.full(shape=(nlines,nfeaturest),fill_value=2)
    labels = np.empty(shape=nlines,dtype=int)
    features_list = ['']*nfeaturest

    iref=nfeaturesc
    features_list[0:iref] = [ \
                     'Len_S1','Len_S2','Len_LDiff', \
                     'MFE_S1','MFE_S2','MFE_S1S2', \
                     'Num_A1A2','Num_C1C2','Num_G1G2','Num_T1T2', \
                     'Num_A1T2','Num_A2T1','Num_C1G2','Num_C2G1', \
                     'Type1_S1','Type1_S2','Type1_Diff', \
                     'Type6_S1','Type6_S2','Type6_Diff', \
                     'TypeSum_Diff','SimScore','SimRatio', \
                     'SumCompKmers','SumMatchKmers']
    features_list[iref:iref+nkv] = ('Comp_Kmer' + str(i) for i in range(nkv))
    iref+=nkv
    features_list[iref:iref+nkv] = ('Match_Kmer' + str(i) for i in range(nkv))
    kmerchar, kmercref = obtain_kmer5()
    
    with open(file1) as f1, open(file2) as f2, open(file3) as f3:
        csv_reader1 = csv.reader(f1,delimiter=',')
        csv_reader2 = csv.reader(f2,delimiter=',')
        csv_reader3 = csv.reader(f3,delimiter=',')
        
        lcount = 0
        for row in csv_reader1:
 
            len1=int(row[2])
            len2=int(row[3])
            row2 = list(map(int,next(csv_reader2)))
            row3 = list(map(int,next(csv_reader3)))
            labels[lcount] = row2[-1]

            # Length of sequences
            iref = 0
            features[lcount,iref] = len1
            
            iref += 1
            features[lcount,iref] = len2
            
            iref += 1
            features[lcount,iref] = np.log(abs(len2-len1)+1)

            # Free energy
            iref += 1
            (ss,mfe1) = RNA.fold(row[0])
            features[lcount,iref] = mfe1

            iref += 1
            (ss,mfe2) = RNA.fold(row[1])
            features[lcount,iref] = mfe2
                
            iref += 1
            features[lcount,iref] = mfe1*mfe2

            # Number of A-A, C-C, G-G, and T-T elements
            iref += 1
            features[lcount,iref] = (row[0].count('A'))*(row[1].count('A'))
            
            iref += 1
            features[lcount,iref] = (row[0].count('C'))*(row[1].count('C'))

            iref += 1
            features[lcount,iref] = (row[0].count('G'))*(row[1].count('G'))
            
            iref += 1
            features[lcount,iref] = (row[0].count('T'))*(row[1].count('T'))
            
            # Number of A-T and C-G elements
            iref += 1
            features[lcount,iref] = (row[0].count('A'))*(row[1].count('T'))

            iref += 1
            features[lcount,iref] = (row[1].count('A'))*(row[0].count('T'))

            iref += 1
            features[lcount,iref] = (row[0].count('C'))*(row[1].count('G'))

            iref += 1
            features[lcount,iref] = (row[1].count('C'))*(row[0].count('G'))

            # Types:
            iref += 1
            features[lcount,iref] = row2[2048]

            iref += 1
            features[lcount,iref] = row2[2057]

            iref += 1
            features[lcount,iref] = abs(row2[2057]-row2[2048])

            iref += 1
            features[lcount,iref] = row2[2053]

            iref += 1
            features[lcount,iref] = row2[2062]

            iref += 1
            features[lcount,iref] = abs(row2[2062]-row2[2053])

            iref += 1
            features[lcount,iref] = abs(sum(row[2048:2057]) \
                                       -sum(row[2057:2066]))

            # String similarity
            iref += 1
            features[lcount,iref] = lev.distance(row[0],row[1])

            iref += 1
            features[lcount,iref] = lev.ratio(row[0],row[1])

            # Matching kmers
            mkmer1 = np.empty(nkv)
            mkmer2 = np.empty(nkv)
            for i1 in range(nkv):
                mkmer1[i1] = row2[i1]*row2[kmercref[i1]+nkv]
                mkmer2[i1] = row2[i1]*row2[i1+nkv]

            iref += 1
            features[lcount,iref] = sum(mkmer1)

            iref += 1
            features[lcount,iref] = sum(mkmer2)

            iref += 1                
            i1=iref+nkv
            features[lcount,iref:i1] = mkmer1[:]

            iref += nkv
            i1=iref+nkv
            features[lcount,iref:i1] = mkmer2[:]

            # Deep Bind representation, first sequence
            #iref = i1+nkv
            #sref = 0
            #irefp = min([nml,len1])
            #for ii in range(4):
            #    features[lcount,iref:iref+irefp] = row3[sref:sref+irefp]
            #    iref += nml
            #    sref += len1
            
            # Deep Bind representation, second sequence
            #irefp = min([nml,len2])
            #for ii in range(4):
            #    features[lcount,iref:iref+irefp] = row3[sref:sref+irefp]
            #    iref += nml
            #    sref += len2
            
            # Increment row to save
            lcount += 1

    return features, features_list, labels


# ============================================================================

# Train the model
if (training_now):
    # Get the training data features
    features, features_list, labels = \
        obtain_features(frawtrain,fkmertrain,fdeeptrain)
       
    # Test parameters
    if (parameters_now):
        rfc = RandomForestClassifier(n_estimators=100)
        cv = ShuffleSplit(n_splits=3, test_size=0.2, random_state=0)
        parameters = {
                  'criterion': ['entropy','gini'],
                  'max_depth': [None,10,100,1000],
                  'min_samples_split': [0.001,0.01,2],
                  'min_samples_leaf': [1,10,100,1000],
                  'min_weight_fraction_leaf': [0,0.1,0.2,0.3,0.4,0.5],
                  'max_features': ['auto','sqrt','log2',None],    
                  'max_leaf_nodes': [None,10,100,1000],
                  'min_impurity_decrease': [0.,0.01,0.1]
                  }
        rfa = GridSearchCV(rfc, parameters, cv=cv)
        rfa.fit(features,labels)
        rf = rfa.best_estimator_
    else:
        # Instantiate random forest classifier
        rf = RandomForestClassifier(n_estimators=400,
             criterion='entropy')
    
    # Train the model on training data
    rf.fit(features,labels)

    # Save the model
    dump(rf, frfm)
else:
    # Load the model
    rf = load(frfm)

# Test the model and make predictions
featurest, features_listt, labelst = \
    obtain_features(frawtest,fkmertest,fdeeptest)
predictions = rf.predict(featurest)

# Write out predictions to files
frraw  = open(frawresult,mode='w')
frkmer = open(fkmerresult,mode='w')
frdeep = open(fdeepresult,mode='w')

for i in predictions:
    if (i == 0):
        line = '-\n'
    else:
        line = '+\n'
    frraw.write(line)
    frkmer.write(line)
    frdeep.write(line)

frraw.close()
frkmer.close()
frdeep.close()


# Results
if vaccuracy_now:
    # Calculate the absolute errors
    wrong = abs(predictions - labelst)

    # Print out the accuracy
    test_count = labelst.shape[0]
    print('Accuracy: ',(test_count - sum(wrong))/test_count*100.)

# Get numerical feature importances
if vimportance_now:
    importances = list(rf.feature_importances_)

    # List of tuples with variable and importance
    feature_importances = [(featurest, round(importances,2)) for featurest, 
                       importances in zip(features_listt, importances)]

    # Sort the feature importances by most important first
    feature_importances = sorted(feature_importances, key=lambda x:x[1], 
                             reverse=True)

    # Print out the feature and importances
    [print('Variable: {:20} Importance: {}'.format(*pair)) for pair in 
     feature_importances[0:20]];
 
# Look at a small tree
if vbranch_now:
    rf_small = RandomForestClassifier(n_estimators=10, max_depth=3)
    rf_small.fit(featurest,predictions)

    # Extract the small tree
    tree_small = rf_small.estimators_[5]
 
    # Save tree as png image
    export_graphviz(tree_small, out_file=ftreedot,
       feature_names=features_listt, rounded=True, precision=1)

    import pydot
    (graph,) = pydot.graph_from_dot_file(ftreedot)
    graph.write_png(ftreepng);
