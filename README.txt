###############################################################################
Predicting RNA Interaction
###############################################################################

This project contains two directories:
- docs: Contains program description.
- python: Contains rna_interact.py program and input files.


Program rna_interact.py predicts if two RNA sequences interact using a
random forest.

INPUT: Two RNA sequences provided in three different formats:
seq_test.csv, kmer_test.csv, and deepbind_test.csv.
OUTPUT: '+' (positive) or '-' (negative) per line matching the test input,
written into three files: seq_test.out, kmer_test.out, and deepbind_test.out.

Python non-standard module requirements:
  - sklearn : on my Anaconda distribution, this installed with:
      conda install scikit-learn
      ("pip install -U scikit-learn" should also work)
  - Levenshtein (e.g., pip install python-levenshtein)
  - ViennaRNA / RNAlib : on my Anaconda distribution, this installed with:
      conda install -c bioconda viennarna
  - joblib : on my Anaconda distribution, this installed with:
      conda install joblib
      ("pip install joblib" should also work)
      
Optional modules (not required to produce the output text files; only if
  visualization flags are enabled):
  - pydot : on my Anaconda distribution, this installed with:
      conda install pydot
      ("pip install pydot" should also work)
  
!!The trained model is NOT provided in this repository!!
Please see below for how to simply fix the situation by training the model
using the code and training data I have provided, thus producing ~60% accurate
results on your system (based on a training/testing ratio of 80/20 from the
provided data).

For this program, reading in the files uses csv, the random forest
implementation relies on numpy, and viewing options to investigate the
performance relies on pydot.  The features selected try to use as much
information as could be easily determined from the RNA sequences provided
without being an expert in RNA biology.  The features use string similarity
from the Levenshtein packages as well as free entropy from the RNA package
(ViennaRNA).  

#########################################################
HOW TO TRAIN ON YOUR PLATFORM

The three required training files are available at:
https://bit.ly/335b1rB

In the program, please switch the training_now switch to
True (line 20). The program will train on the saved training data
provided in this directory (~40 min).  Once trained, it will
immediately continue on to make predictions based on the test data
contained in the input test files named above, writing the
predictions to the three output files.

#################################
MORE ON TRAINING / VISUALIZATION

Accuracy:
The accuracy of tests can be output if vaccuracy_now is switched to True.
This uses the last element in the kmer_test.csv file as the true labels.

Variable Importance:
The importance of each of the features will be written out to the screen
if vimportance_now is switched to True.

Sample Tree Diagram:
A png image showing a sample tree is saved to small_tree.png
if vbranch_now is switched to True.

Parameter Estimation:
During training, various parameter values were tested using GridSearchCV and
ShuffleSplit.  The program currently runs using the optimal results found.
A flag controls running the parameter testing, and will execute if
parameters_now is switched to True.


###############################################################################
Katherine Haynes
CS440 Assignment 5
2019/04/29